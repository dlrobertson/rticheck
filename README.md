## RTICheck
### Run-Time Interpositioning to Detect Memory Leaks

The purpose of the project was 1) to play around with interpositioning
and 2) to use run-time interpositioning to write wrappers for `malloc`
and `free` that keep track of the calls to the respective functions
for a rudamentary method to detect memory leaks.

## Install

```
    $ git clone https://gitlab.com/drobertson/rticheck.git
    $ cd rticheck
    $ make
    # make install
```

## Usage

```
    $ rticheck --exec=./good <args>
    Heap allocation stats:
    ======================
    Bytes Alloc'd:  45000
    Mallocs:        1000
    Frees:          1000
    -----------
    No leaks detected!

    $ rticheck --exec=./bad <args>
    Heap allocation stats:
    ======================
    Bytes Alloc'd:  45000
    Mallocs:        1000
    Frees:          998
    -----------
    Possible leaks detected!
```

## A few things to keep in mind when using

1. [valgrind](http://valgrind.org/) [memcheck](http://valgrind.org/docs/manual/mc-manual.html) is a much better and well tested memory error detection tool
2. at the moment this tool only tracks `malloc` and `free`
3. as mentioned previously, this was just a "test" project to get experience with interpositioning
4. See something wrong? Think this is terrible? Please send me feedback! I love writing code, and want to be better, so critiques are very welcome!

## Contibuting

See [Contributing.md](Contributing.md)