# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

PROJECT_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
SRCS_DIR=$(PROJECT_DIR)/src
INSTALL_DIR=/usr/local
export MAJOR_V=0
export MINOR_V=0
export PATCH_V=1
export TARGET_DIR=$(PROJECT_DIR)target
export PROJECT_NAME=rticheck

all:
	$(MAKE) -C $(SRCS_DIR)

install:
	mkdir -p $(INSTALL_DIR)/lib/$(PROJECT_NAME)
	cp $(PROJECT_DIR)target/lib/* $(INSTALL_DIR)/lib/$(PROJECT_NAME)
	#cp $(PROJECT_DIR)rti-mtest $(INSTALL_DIR)/bin/rti-mtest

clean:
	$(MAKE) -C $(SRCS_DIR) clean
