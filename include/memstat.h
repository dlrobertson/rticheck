/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef RTI_MTEST_MEMSTAT_H
#define RTI_MTEST_MEMSTAT_H

struct MemStat {
    int nMalloc;
    int nFree;
    long nBytesMalloc;
};

#endif
