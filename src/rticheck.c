/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>

void help(int extstat) {
    fprintf(stdout, "%s\n", "Usage: rti-mtest --exec=./<executable> <args>");
    exit(extstat);
}

void enderror(char* errstring) {
    fprintf(stderr, "%s\n", errstring);
    exit(1);
}

int main(int argc, char** argv) {
    if(argc < 2) {
        help(1);
    }

    char** arg;
    char** last_arg = argv + argc;
    char exec[255];

    for(arg = argv + 1; arg < last_arg; ++arg) {
        if(strncmp(*arg, "--exec=", 7) == 0) {
            int len = strlen(*arg);
            char* arg_first;
            char* exec_first = exec;
            char* arg_last = (*arg) + len;
            for(arg_first = (*arg) + 7; arg_first < arg_last; ++arg_first, ++exec_first) {
                *exec_first = *arg_first;
            }
            *arg = exec;
            argv = arg;
            break;
        } else if(strcmp(*arg, "--help") == 0) {
            help(0);
        } else {
            fprintf(stderr, "%s: %s\n", "Could not parse user input", *arg);
            help(1);
        }
    }

    char *const envp[] = {"LD_PRELOAD=/usr/local/lib/rticheck/librticheck.so",NULL};
    execve(exec, argv, envp);

    return 0;
}
