/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

#include "memstat.h"

struct MemStat mem_stat = {0, 0};

void* malloc(size_t size) {
    void* (*mallocp)(size_t);
    char* err;

    mallocp = dlsym(RTLD_NEXT, "malloc");
    if((err = dlerror()) != NULL) {
        fprintf(stderr, "%s", err);
        exit(1);
    }
    char* ptr = mallocp(size);

    // Our added methods
    ++mem_stat.nMalloc;
    mem_stat.nBytesMalloc += size;
    return ptr;
}

void free(void *ptr) {
    void* (*freep)(void*) = NULL;
    char* err;

    if(!ptr)
        return;

    freep = dlsym(RTLD_NEXT, "free");
    if((err = dlerror()) != NULL) {
        fprintf(stderr, "%s", err);
        exit(1);
    }
    freep(ptr);

    // Our added methods
    ++mem_stat.nFree;
}
