/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <stdio.h>

#include "memstat.h"

struct MemStat mem_stat;

__attribute__((destructor(101))) void endfunc() {
    fprintf(stdout, "%s\n", "Heap allocation stats:");
    fprintf(stdout, "%s\n", "======================");
    fprintf(stdout, "\tBytes Alloc'd:\t%ld\n", mem_stat.nBytesMalloc);
    fprintf(stdout, "\tMallocs:\t%d\n", mem_stat.nMalloc);
    fprintf(stdout, "\tFrees:\t\t%d\n", mem_stat.nFree);
    fprintf(stdout, "\t%s\n", "-----------");
    if(mem_stat.nFree != mem_stat.nMalloc) {
        fprintf(stdout, "%s", "\tPossible leaks detected!\n");
    } else {
        fprintf(stdout, "%s", "\tNo leaks detected!\n");
    }
}
